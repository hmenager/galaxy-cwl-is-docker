#FROM ubuntu:18.04
FROM billyteves/ubuntu-dind:16.04
WORKDIR /app
RUN apt-get update && apt-get install -y \
  git \
  python-dev \
  python-pip \
  gcc \
  make \
  docker.io \
  wget \
  curl \ 
  libpq-dev \
  vim
RUN git clone --depth 1 --progress https://github.com/common-workflow-language/galaxy.git /app/galaxy
RUN git clone --depth 1 --single-branch --branch master --progress https://github.com/EBI-metagenomics/workflow-is-cwl /app/workflow-is-cwl
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn
# init Galaxy (download deps and init DB...)
RUN cd /app/galaxy && ./scripts/common_startup.sh 
#RUN cd /app/galaxy && . .venv/bin/activate && cwltool --pack /app/workflow-is-cwl/tools/Diamond/Diamon.blastx-v0.9.21.cwl > /app/workflow-is-cwl/tools/Diamond/Diamon.blastx-v0.9.21.packed.cwl
RUN cd /app/galaxy && ./create_db.sh
RUN cd /app/galaxy && ./manage_db.sh upgrade
# DEBUG complex types
COPY representation.py /app/galaxy/lib/galaxy/tools/cwl/representation.py
COPY parser.py /app/galaxy/lib/galaxy/tools/cwl/parser.py
#COPY __init__.py /app/galaxy/lib/galaxy/tools/__init__.py
COPY process.py /app/galaxy/.venv/local/lib/python2.7/site-packages/cwltool/process.py 
COPY wrappers.py /app/galaxy/lib/galaxy/tools/wrappers.py
# local config
COPY job_conf.xml /app/galaxy/config/job_conf.xml
COPY dependency_resolvers_conf.xml /app/galaxy/config/dependency_resolvers_conf.xml
COPY galaxy.yml /app/galaxy/config/galaxy.yml
COPY tool_conf.xml /app/galaxy/config/tool_conf.xml
COPY welcome.html /app/galaxy/static/welcome.html
# create entry point
COPY start.sh /app/start.sh
# test modified gx annotation for type
COPY Diamon.makedb-v0.9.21.cwl /app/workflow-is-cwl/tools/Diamond/Diamon.makedb-v0.9.21.cwl
COPY create_user.py /app/galaxy/create_user.py
# copy a fixed version of workflows.py to allow Workflow API uploads until
# https://github.com/common-workflow-language/galaxy/pull/105 is merged
COPY workflows.py /app/galaxy/lib/galaxy/managers/workflows.py
# create the admin user
RUN bash -c "cd /app/galaxy && . .venv/bin/activate && python create_user.py"
# upload workflows and data
RUN git clone https://github.com/vishnubob/wait-for-it.git
RUN cd /app/galaxy && . .venv/bin/activate && pip install bioblend
COPY upload_material.py /app
COPY start_galaxy_and_upload_material.sh /app
RUN bash /app/start_galaxy_and_upload_material.sh
CMD ["sh", "/app/start.sh"]
EXPOSE 8080
