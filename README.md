Galaxy CWL IS Docker
====================

This repository contains the material to build a Galaxy test server for the ELIXIR Workflow Interoperability Implementation Study.

It can be used to test the different CWL tools and workflows developed in the context of the [Enabling the reuse, extension, scaling, and reproducibility of scientific workflows (2018-cwl) implementation study](https://elixir-europe.org/about-us/implementation-studies/cwl-2018).
The source of the CWL tools and workflows is available on the [ELIXIR Workflow Implementation Study CWL descriptions github repository](https://github.com/EBI-Metagenomics/workflow-is-cwl/).

The preconfigured admin user for this server is `galaxy@commonwl.org`, its password is `galaxy`.

**This material is for demonstration purposes only, DO NOT USE IT IN PRODUCTION, it is not safe and stable enough!**

How to build: 

```bash
docker build . -t galaxycwlis
```

How to run the server locally: 

```bash
docker run --privileged -i -t -p 8080:8080 galaxycwlis:latest
```

How to open a terminal on the server: 

```bash
docker container ls

docker exec -i -t [CONTAINER_NAME] bash
```
