class: CommandLineTool
cwlVersion: v1.0
baseCommand:
  - cat
inputs:
  - id: files
    type: 'File'
    inputBinding:
      position: 1
    streamable: true
outputs:
  - id: result
    type: File
    outputBinding:
      glob: result
      outputEval: |
        ${ self[0].format = inputs.files.format;
           return self; }
requirements:
  - class: ResourceRequirement
    ramMin: 100
    coresMax: 1
  - class: InlineJavascriptRequirement
stdout: result
